#pragma once
struct Particle
{
	float x;
	float y;
	float z;
	float radius;
	float r;
	float g;
	float b;
	Particle(float x, float y, float z, float radius, float r, float g, float b) :x(x), y(y), z(z), radius(radius), r(r), g(g), b(b) {}
};