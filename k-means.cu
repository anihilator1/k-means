

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include<iostream>
#include<list>
#include<vector>
#include"particle.h"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/count.h>
#include <thrust/reduce.h>
#include<thrust/transform.h>

static void CheckCudaErrorAux(const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

static void CheckCudaErrorAux(const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;
	std::cerr << statement << " returned " << cudaGetErrorString(err) << "(" << err << ") at " << file << ":" << line << std::endl;
	exit(1);
}


__device__ float calculateDistance(ParticleForDevice particleA, ParticleForDevice particleB)
{
	float result = (particleA.x - particleB.x)*(particleA.x - particleB.x) + (particleA.y - particleB.y)*(particleA.y - particleB.y) + (particleA.z - particleB.z)*(particleA.z - particleB.z);
	return result;
}
__device__ void globalToShared(ParticleForDevice* global, ParticleForDevice* shared, unsigned idb, unsigned blockDim, unsigned size)
{
	int index = idb;
	if (idb < size)
	{
		shared[index] = global[index];
		index += blockDim;
		size -= blockDim;
	}
}
__global__ void groupParticles(ParticleForDevice* particlesData, unsigned  paticlesDataSize, ParticleForDevice* centoridsData, unsigned centroidsDataSize, int* resultData)
{
	unsigned idx = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned idb = threadIdx.x;
	extern __shared__ ParticleForDevice centoridsDataShared[];
	globalToShared(centoridsData, centoridsDataShared, idb, blockDim.x, centroidsDataSize);
	if (idx < paticlesDataSize)
	{
		float min = calculateDistance(centoridsData[0], particlesData[idx]);
		resultData[idx] = 0;
		for (int i = 1; i < centroidsDataSize; i++)
		{
			float result = calculateDistance(centoridsData[i], particlesData[idx]);
			if (result < min)
			{
				min = result;
				resultData[idx] = i;
			}
		}
	}
}
ParticleForDevice* vectorToArray(std::vector<Particle> particleList)
{
	ParticleForDevice* tab = new ParticleForDevice[particleList.size()];
	int index = 0;
	for (auto it = particleList.begin(); it != particleList.end(); ++it)
	{
		tab[index] = (*it).getDeviceParticle();
		index++;
	}
	return tab;
}
void printTab(int *tab, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << tab[i] << " ";
	}
}
std::vector<int> arrayToVector(int * tab, int size)
{
	std::vector<int> resultList;
	for (int i = 0; i < size; i++)
	{
		resultList.push_back(tab[i]);
	}
	return resultList;
}
std::vector<int> initKernel(std::vector<Particle> particlesList, std::vector<Particle> centroidsList)
{
	int particlesDataSize = particlesList.size();
	int centroidsDataSize = centroidsList.size();
	int blockSize = 64;
	int blockCount = (particlesDataSize + blockSize - 1) / blockSize;

	ParticleForDevice* hostDataParticles = vectorToArray(particlesList);
	ParticleForDevice* hostDataCentroids = vectorToArray(centroidsList);
	int* hostDataResult = new int[particlesDataSize];

	ParticleForDevice* deviceDataParticles;
	ParticleForDevice* deviceDataCentroids;
	int* deviceDataResult;

	CUDA_CHECK_RETURN(cudaMalloc((void **)&deviceDataParticles, sizeof(ParticleForDevice)*particlesDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&deviceDataCentroids, sizeof(ParticleForDevice)*centroidsDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&deviceDataResult, sizeof(int)*particlesDataSize));

	CUDA_CHECK_RETURN(cudaMemcpy(deviceDataParticles, hostDataParticles, sizeof(ParticleForDevice)*particlesDataSize, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(deviceDataCentroids, hostDataCentroids, sizeof(ParticleForDevice)*centroidsDataSize, cudaMemcpyHostToDevice));

	groupParticles << < blockCount, blockSize, centroidsDataSize * sizeof(ParticleForDevice) >> > (deviceDataParticles, particlesDataSize, deviceDataCentroids, centroidsDataSize, deviceDataResult);

	CUDA_CHECK_RETURN(cudaMemcpy(hostDataResult, deviceDataResult, sizeof(int)*particlesDataSize, cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaFree(deviceDataParticles));
	CUDA_CHECK_RETURN(cudaFree(deviceDataCentroids));
	CUDA_CHECK_RETURN(cudaFree(deviceDataResult));

	return arrayToVector(hostDataResult, particlesDataSize);
}
thrust::device_vector<ParticleForDevice> vectorToDeviceVector(std::vector<Particle> vector)
{
	thrust::host_vector<ParticleForDevice> hostVector;
	for (int i = 0; i < vector.size(); i++)
	{
		auto tmp = vector[i];
		auto tmp1 = vector[i].getDeviceParticle();
		hostVector.push_back(tmp1);
	}
	thrust::device_vector<ParticleForDevice> deviceVector = hostVector;
	return deviceVector;
}
thrust::device_vector<int> vectorToDeviceVector(std::vector<int> vector)
{
	thrust::device_vector<int> deviceVector;
	for (int i = 0; i < vector.size(); i++)
	{
		deviceVector.push_back(vector[i]);
	}
	return deviceVector;
}

struct ParticlesForCentroid
{
	int centroid;
	ParticlesForCentroid(int centroid) :centroid(centroid) {}
	__device__ ParticleForDevice operator()(thrust::tuple<ParticleForDevice, int> tuple)
	{
		int tmp = thrust::get<1>(tuple);
		if (centroid == tmp)
		{
			return thrust::get<0>(tuple);
		}
		else
		{
			return ParticleForDevice(0, 0, 0);
		}
	}
};
struct AddFunc
{
	__device__ ParticleForDevice operator()(ParticleForDevice particle1, ParticleForDevice particle2)
	{
		return ParticleForDevice(particle1.x + particle2.x, particle1.y + particle2.y, particle1.z + particle2.z);
	}
};

std::vector<ParticleForDevice> computeNewPosition(std::vector<Particle> particles, int centroidsNumber, std::vector<int> groups)
{
	thrust::device_vector<ParticleForDevice> particlesDeviceVector = vectorToDeviceVector(particles);
	thrust::device_vector<int> groupsDeviceVector = groups;
	auto zipIteratorBegin = thrust::make_zip_iterator(thrust::make_tuple(particlesDeviceVector.begin(), groupsDeviceVector.begin()));
	auto zipIteratorEnd = thrust::make_zip_iterator(thrust::make_tuple(particlesDeviceVector.end(), groupsDeviceVector.end()));
	thrust::tuple<ParticleForDevice, int> init = thrust::make_tuple(ParticleForDevice(0, 0, 0), -1);
	std::vector<ParticleForDevice> resultVector(centroidsNumber);
	for (int i = 0; i < centroidsNumber; i++)
	{

		ParticleForDevice init = ParticleForDevice(0, 0, 0);
		
		thrust::device_vector<ParticleForDevice> centroidParticles(particles.size());

		ParticlesForCentroid func1(i);
		thrust::transform(zipIteratorBegin, zipIteratorEnd, centroidParticles.begin(), func1);
		
		AddFunc func2;
		ParticleForDevice particle = thrust::reduce(centroidParticles.begin(), centroidParticles.end(), init, func2);

		int count = thrust::count(groupsDeviceVector.begin(), groupsDeviceVector.end(), i);
		particle.x /= count;
		particle.y /= count;
		particle.z /= count;
		resultVector[i] = particle;
	}
	return resultVector;
}