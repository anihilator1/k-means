#pragma once
#include "partciles-for-device.h"
struct Particle
{
	float x;
	float y;
	float z;
	float radius;
	float r;
	float g;
	float b;
	Particle() {}
	Particle(float x, float y, float z, float radius, float r, float g, float b) :x(x), y(y), z(z), radius(radius), r(r), g(g), b(b) {}
	ParticleForDevice getDeviceParticle()
	{
		return ParticleForDevice(x, y, z);
	}
	void setPostion(ParticleForDevice particle)
	{
		x = particle.x;
		y = particle.y;
		z = particle.z;
	}
};