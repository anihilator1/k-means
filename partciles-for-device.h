#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
__host__ __device__ struct ParticleForDevice
{
	float x;
	float y;
	float z;

	__host__ __device__  ParticleForDevice() {}
	__host__ __device__ ParticleForDevice(float x, float y, float z) :x(x), y(y), z(z) {}
};