#include<stdio.h>
#include<iostream>

#include <helper_gl.h>
#if defined (WIN32)
#include <GL/wglew.h>
#endif
#if defined(__APPLE__) || defined(__MACOSX)
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#include <GLUT/glut.h>
#ifndef glutCloseFunc
#define glutCloseFunc glutWMCloseFunc
#endif
#else
#include <GL/freeglut.h>
#endif

#include"particle.h"
#include<vector>
#include "k-means.cuh"
float camera_trans[] = { 0, 0, -3 };
float camera_rot[] = { 0, 0, 0 };
float camera_trans_lag[] = { 0, 0, -3 };
float camera_rot_lag[] = { 0, 0, 0 };
const float inertia = 0.1f;
float modelView[16];
int ox, oy;
int buttonState = 0;
float particleRadius = 0.01f;
int particleNumber = 2000;
std::vector<Particle> particleList;
std::vector<Particle> centroidList;

void initGL(int *argc, char **argv)
{
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(800, 600);
	glutCreateWindow("CUDA Particles");

	if (!isGLVersionSupported(2, 0) ||
		!areGLExtensionsSupported("GL_ARB_multitexture GL_ARB_vertex_buffer_object"))
	{
		fprintf(stderr, "Required OpenGL extensions missing.");
		exit(EXIT_FAILURE);
	}

#if defined (WIN32)

	if (wglewIsSupported("WGL_EXT_swap_control"))
	{
		// disable vertical sync
		wglSwapIntervalEXT(0);
	}

#endif

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.25, 0.25, 0.25, 1.0);

	glutReportErrors();
}
void idle(void)
{
	glutPostRedisplay();
}
void reshape(int w, int h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (float)w / (float)h, 0.1, 100.0);

	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, w, h);

}
void mouse(int button, int state, int x, int y)
{
	int mods;

	if (state == GLUT_DOWN)
	{
		buttonState |= 1 << button;
	}
	else if (state == GLUT_UP)
	{
		buttonState = 0;
	}

	mods = glutGetModifiers();

	if (mods & GLUT_ACTIVE_SHIFT)
	{
		buttonState = 2;
	}
	else if (mods & GLUT_ACTIVE_CTRL)
	{
		buttonState = 3;
	}

	ox = x;
	oy = y;

	glutPostRedisplay();
}
void motion(int x, int y)
{
	float dx, dy;
	dx = (float)(x - ox);
	dy = (float)(y - oy);

	if (buttonState == 3)
	{
		// left+middle = zoom
		camera_trans[2] += (dy / 100.0f) * 0.5f * fabs(camera_trans[2]);
	}
	else if (buttonState & 2)
	{
		// middle = translate
		camera_trans[0] += dx / 100.0f;
		camera_trans[1] -= dy / 100.0f;
	}
	else if (buttonState & 1)
	{
		// left = rotate
		camera_rot[0] += dy / 5.0f;
		camera_rot[1] += dx / 5.0f;
	}

	ox = x;
	oy = y;

	glutPostRedisplay();
}
void key(unsigned char key, int /*x*/, int /*y*/)
{
	if (key == 'n')
	{
		std::vector<int> result = initKernel(particleList, centroidList);
		int index = 0;
		for (auto it = particleList.begin(); it != particleList.end(); ++it)
		{
			(*it).r = centroidList[result[index]].r;
			(*it).g = centroidList[result[index]].g;
			(*it).b = centroidList[result[index]].b;
			index++;
		}
		std::vector<ParticleForDevice> list = computeNewPosition(particleList, centroidList.size(), result);
		for (int i = 0; i < list.size(); i++)
		{
			centroidList[i].setPostion(list[i]);
		}
	}
}
float frand()
{
	return rand() / (float)RAND_MAX;
}
float randPosition()
{
	return (frand() - 0.5f) * (2 - particleRadius*2);
}
void setParticle(Particle particle)
{
	glPushMatrix();
	glColor3f(particle.r,particle.g,particle.b);
	glTranslatef(particle.x, particle.y, particle.z);
	glutSolidSphere(particle.radius, 20, 50);
	glPopMatrix();
}
void display()
{


	// render
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// view transform
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	for (int c = 0; c < 3; ++c)
	{
		camera_trans_lag[c] += (camera_trans[c] - camera_trans_lag[c]) * inertia;
		camera_rot_lag[c] += (camera_rot[c] - camera_rot_lag[c]) * inertia;
	}

	glTranslatef(camera_trans_lag[0], camera_trans_lag[1], camera_trans_lag[2]);
	glRotatef(camera_rot_lag[0], 1.0, 0.0, 0.0);
	glRotatef(camera_rot_lag[1], 0.0, 1.0, 0.0);

	glGetFloatv(GL_MODELVIEW_MATRIX, modelView);

	glColor3f(1.0, 1.0, 1.0);
	glutWireCube(2.0);
	for (auto it = particleList.begin(); it != particleList.end(); ++it)
	{
		setParticle(*it);
	}
	for (auto it = centroidList.begin(); it != centroidList.end(); ++it)
	{
		setParticle(*it);
	}
	glutSwapBuffers();
	glutReportErrors();

}
void initParticleVector()
{
	for(int i=0;i<particleNumber;i++)
		particleList.push_back(Particle(randPosition(), randPosition(), randPosition(), particleRadius, 1, 1, 0));
}
void initWindow(int* argc,char **argv)
{
	initGL(argc, argv);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutIdleFunc(idle);
	glutKeyboardFunc(key);
	glutMainLoop();
}
void initCentroidVector()
{
	centroidList.push_back(Particle(randPosition(), randPosition(), randPosition(), 0.05, 1, 0, 0));
	centroidList.push_back(Particle(randPosition(), randPosition(), randPosition(), 0.05, 0, 1, 0));
	centroidList.push_back(Particle(randPosition(), randPosition(), randPosition(), 0.05, 0, 0, 1));
}
void initComputing()
{
	initParticleVector();
	initCentroidVector();
}
int main(int argc, char**argv)
{
	initComputing();
	std::cout << "ok";
	initWindow(&argc, argv);
	std::cout << "ok";
}