#pragma once
std::vector<int> initKernel(std::vector<Particle> particlesList, std::vector<Particle> centroidsList);
std::vector<ParticleForDevice> computeNewPosition(std::vector<Particle> particles, int centroidsNumber, std::vector<int> groups);

